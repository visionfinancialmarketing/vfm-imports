<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('parent_id');
            $table->boolean('master');
            $table->boolean('disabled');
            $table->text('first_name');
            $table->text('last_name');
            $table->text('title');
            $table->text('email');
            $table->text('password');
            $table->dateTime('password_expiration');
            $table->text('website');
            $table->text('phone_number');
            $table->text('fax_number');
            $table->text('business_name');
            $table->text('address');
            $table->text('address_line_2');
            $table->text('city');
            $table->text('state');
            $table->text('zip');
            $table->text('timezone');
            $table->unsignedBigInteger('emails_sent');
            $table->unsignedInteger('automatic_logout');
            $table->text('default_calendar_view');
            $table->time('calendar_start_time');
            $table->time('calendar_end_time');
            $table->unsignedInteger('activity_default_time');
            $table->text('headshot');
            $table->text('signature');
            $table->text('logo');
            $table->text('category_colors');
            $table->text('option_colors');
            $table->longText('email_signature');
            $table->longText('compliance');
            $table->longText('cart');
            $table->longText('preferences');
            $table->unsignedInteger('appointment_number_max');
            $table->boolean('deleted');
            $table->unsignedInteger('renewal_day_of_month');
            $table->dateTime('deleted_date');
            $table->unsignedInteger('billing_paid_account');
            $table->unsignedInteger('billing_users_count');
            $table->boolean('auto_renew_notification');
            $table->text('billing_membership');
            $table->unsignedInteger('billing_space_tier');
            $table->unsignedInteger('billing_email_tier');
            $table->date('billing_next_date');
            $table->date('billing_next_date_previous');
            $table->date('billing_last_date');
            $table->decimal('billing_last_amount', 10, 2);
            $table->boolean('billing_account_graced');
            $table->dateTime('last_failed_email_notice');
            $table->boolean('last_failed_email_notice_flag');
            $table->dateTime('near_quota_email_notice');
            $table->boolean('near_quota_email_notice_flag');
            $table->dateTime('unverified_email_notice');
            $table->boolean('unverified_email_notice_flag');
            $table->unsignedInteger('lock_count');
            $table->dateTime('lock_timestamp');
            $table->string('remember_id', 128);
            $table->string('remember_hash', 128);
            $table->string('remember_key', 128);
            $table->dateTime('remember_expires');
            $table->string('reset_link', 128);
            $table->dateTime('reset_expires');
            $table->boolean('verified');
            $table->string('verify_link', 128);
            $table->boolean('active')->default(1);
            $table->unsignedInteger('boldness')->default(300);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
