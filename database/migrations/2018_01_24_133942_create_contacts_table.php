<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('created_by');
            $table->string('classification', 32);
            $table->unsignedInteger('rating');
            $table->string('status', 32);
            $table->unsignedBigInteger('email_address');
            $table->unsignedBigInteger('telephone_number');
            $table->unsignedBigInteger('street_address');
            $table->text('photo');
            $table->text('first_name');
            $table->text('middle_name');
            $table->text('last_name');
            $table->text('alternate_name');
            $table->text('maiden_name');
            $table->text('prefix');
            $table->text('suffix');
            $table->text('brief_description');
            $table->text('occupation_title');
            $table->text('occupation_company');
            $table->date('occupation_start');
            $table->date('occupation_end');
            $table->text('salary');
            $table->text('note');
            $table->date('birthdate');
            $table->text('birthplace');
            $table->date('date_of_death');
            $table->text('gender');
            $table->text('citizenship_country');
            $table->text('ethnicity');
            $table->text('marital_status');
            $table->date('anniversary_date');
            $table->text('retirement_status');
            $table->date('retirement_date');
            $table->text('tax_id_number');
            $table->text('pets');
            $table->text('dl_number');
            $table->text('dl_state');
            $table->date('dl_expiration_date');
            $table->text('source');
            $table->text('source_category');
            $table->text('source_notes');
            $table->date('client_timestamp');
            $table->dateTime('record_timestamp');
            $table->unsignedInteger('office_primary_advisor');
            $table->unsignedInteger('office_secondary_advisor');
            $table->text('office_primary_csr');
            $table->text('office_secondary_csr');
            $table->text('office_location');
            $table->text('appointment_status');
            $table->text('appointment_number');
            $table->date('hold_date');
            $table->unsignedBigInteger('current_plan');
            $table->mediumText('case_management');
            $table->text('personal_touch_workflow');
            $table->dateTime('personal_touch_timestamp');
            $table->boolean('flagged');
            $table->boolean('wrenched');
            $table->unsignedInteger('num_children');
            $table->string('finances')->nullable();
            $table->string('homeownership')->nullable();
            $table->decimal('fed_taxes', 6, 2)->nullable();
            $table->decimal('state_taxes', 6, 2)->nullable();
            $table->year('tax_year')->nullable();
            $table->decimal('net_worth', 16, 2);
            $table->timestamp('last_access')->nullable();
            $table->boolean('do_not_call')->default(0);
            $table->boolean('do_not_email')->default(0);
            $table->boolean('do_not_mail')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
