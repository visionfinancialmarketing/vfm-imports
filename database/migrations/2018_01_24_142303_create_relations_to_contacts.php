<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationsToContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relations_to_contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('relation_id');
            $table->unsignedBigInteger('contact_id');
            $table->unsignedBigInteger('relative_id');
            $table->text('other_relation');
            $table->text('relative_name');
            $table->date('relative_birthdate');
            $table->text('relative_description');
            $table->boolean('referral');
            $table->date('referral_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relations_to_contacts');
    }
}
