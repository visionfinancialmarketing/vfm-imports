<?php

use Illuminate\Mail\Markdown;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/profile', 'ProfileController@index')->name('profile');
Route::post('profile/update_profile', 'ProfileController@update');

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::resource('crms', 'CRMController');
Route::resource('clients', 'ClientController');
Route::get('clients/{id}/get_account_users', 'ClientController@getAccountUsers');
Route::resource('contacts', 'ContactController');

Route::post('imports/file_import', 'ImportController@storeImports');
Route::get('imports/{crm_id}/show_all_CRM_files', 'ImportController@showImports');
Route::post('imports/remove_file', 'ImportController@destroyImport');
Route::post('imports/import_contacts', 'ImportController@importContacts');
Route::get('imports/reset', 'ImportController@resetLocalImports');
Route::get('imports/{field_type}/get_field_types', 'ImportController@getFieldTypes');


Route::get('/mail/processing', function () {
	$user = Auth::user();
	$markdown = new Markdown(view(), config('mail.markdown'));
	$count = 286;
	$client_name = 'Test Client';

	return $markdown->render('mail.import.processing', ['name' => $user->name, 'contacts_count' => $count, 'client_name' => $client_name]);
});

Route::get('/mail/complete', function () {
	$url = url('/contacts');
	$user = Auth::user();
	$markdown = new Markdown(view(), config('mail.markdown'));
	$count = 286;
	$client_name = 'Test Client';

    return $markdown->render('mail.import.complete', ['url' => $url, 'name' => $user->name, 'contacts_count' => $count, 'client_name' => $client_name]);
});
