@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="fa-layers fa-fw">
                            <i class="fas fa-cloud"></i>
                            <span class="fa-layers-text fa-inverse" data-fa-transform="shrink-10 down-2" style="font-weight:900;color:#f4791e;">CRM</span>
                        </span>
                        CRM's
                        <a href="/crms/create" class="btn btn-primary btn-reverse btn-sm pull-right">Add CRM</a>
                    </div>

                    <div class="panel-body">
                        <ul class="">
                            @forelse ($crms as $crm)
                                <li>
                                    <a href="/crms/{{ $crm->id }}">
                                        {{ $crm->name }}
                                    </a>
                                </li>
                            @empty
                                <li><h1>There are no CRM's at this time.</h1></li>
                            @endforelse
                        </ul>

                        @if ( method_exists($crms, 'links') )
                            {{ $crms->links() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
