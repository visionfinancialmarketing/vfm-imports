<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="/img/vfmlogo.svg" alt="VFM Logo" class="vfm-logo" />
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                @guest
                    <li><a href=""></a></li>
                @else
                    <li>
                        <a href="/dashboard">
                            <i class="far fa-tachometer"></i> Dashboard
                        </a>
                    </li>
                    <li>
                        <a href="/crms">
                            <span class="fa-layers fa-fw">
                                <i class="fas fa-cloud"></i>
                                <span class="fa-layers-text fa-inverse" data-fa-transform="shrink-10 down-2" style="font-weight:900;color:#f4791e;">CRM</span>
                            </span>
                            CRM's
                        </a>
                    </li>
                    <li>
                        <a href="/clients">
                            <i class="far fa-users"></i> Clients
                        </a>
                    </li>
                    <li>
                        <a href="/contacts">
                            <i class="far fa-address-book"></i> Contacts
                        </a>
                    </li>
                @endguest
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @guest
                    <li><a href="{{ route('login') }}">Login</a></li>
                @else
                    @if (Auth::user()->isAdmin())
                    <li>
                        <a href="/imports/reset" class="btn btn-link">
                            <i class="far fa-redo"></i>
                            Reset Imports
                        </a>
                    </li>
                    <li>
                        <a class="btn btn-link" data-toggle="modal" data-target="#modal-import-contacts">
                            <span class="fa-layers fa-fw">
                                <i class="fas fa-cloud"></i>
                                <i class="far fa-address-book" data-fa-transform="shrink-5 down-1" style="color:#f4791e"></i>
                            </span>
                            Import Contacts
                        </a>
                    </li>
                    @endif
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li class="dropdown-header">User</li>

                            <li>
                                <a href="/profile">
                                    <i class="far fa-user"></i>
                                    Profile
                                </a>
                            </li>

                            <li class="divider"></li>

                            <li class="dropdown-header">Emails</li>

                            <li>
                                <a href="/mail/processing">
                                    <i class="far fa-envelope-open"></i>
                                    Processing Email
                                </a>
                            </li>

                            <li>
                                <a href="/mail/complete">
                                    <i class="fas fa-envelope"></i>
                                    Complete Email
                                </a>
                            </li>

                            <li class="divider"></li>

                            <li style="margin-bottom: 10px;">
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    <i class="far fa-sign-out"></i> Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
