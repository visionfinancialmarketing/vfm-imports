@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @if ($client->headshot)
                            <img src="https://www.visionfinancialmarketing.com/dashboard/{{ $client->headshot }}" />
                        @else
                            <img src="https://www.visionfinancialmarketing.com/dashboard/images/profile.png" />
                        @endif
                        {{ $client->first_name }} {{ $client->last_name }}
                    </div>

                    <div class="panel-body">
                        <p><strong>Email</strong>: <a href="mailto:{{ $client->email }}">{{ $client->email }}</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
