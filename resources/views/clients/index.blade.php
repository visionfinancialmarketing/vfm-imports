@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="far fa-users"></i> Clients</div>

                    <div class="panel-body">
                        <ul class="">
                            @forelse ($clients as $client)
                                <li>
                                    <a href="/clients/{{ $client->id }}">
                                        @if ($client->headshot)
                                            <img src="https://www.visionfinancialmarketing.com/dashboard/{{ $client->headshot }}" />
                                        @else
                                            <img src="https://www.visionfinancialmarketing.com/dashboard/images/profile.png" />
                                        @endif
                                        {{ $client->first_name }} {{ $client->last_name }}
                                    </a>
                                </li>
                            @empty
                                <li><h1>There are no clients at this time.</h1></li>
                            @endforelse
                        </ul>

                        @if ( method_exists($clients, 'links') )
                            {{ $clients->links() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
