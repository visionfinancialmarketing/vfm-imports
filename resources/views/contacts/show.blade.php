@extends('layouts.app')

@section('content')
    <contact :contact="{{ $contact }}"></contact>
@endsection
