@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="far fa-address-book"></i> Contacts</div>

                    <div class="panel-body">
                        <ul>
                            @forelse ($contacts as $contact)
                                <li>
                                    <a href="/contacts/{{ $contact->id }}">
                                        @if ($contact->photo)
                                            <img src="https://www.visionfinancialmarketing.com/dashboard/{{ $contact->photo }}" />
                                        @else
                                            <img src="https://www.visionfinancialmarketing.com/dashboard/images/profile.png" />
                                        @endif
                                        {{ $contact->first_name }} {{ $contact->last_name }} ({{ $contact->classification }})
                                    </a>
                                </li>
                            @empty
                                <li style="margin-bottom: 0;">There are no contacts at this time.</li>
                            @endforelse
                        </ul>

                        @if ( method_exists($contacts, 'links') )
                            {{ $contacts->links() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
