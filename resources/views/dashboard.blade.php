@extends('layouts.app')

@section('content')
    <dashboard :user="{{ $user }}" :crms="{{ $crms }}"></dashboard>
@endsection
