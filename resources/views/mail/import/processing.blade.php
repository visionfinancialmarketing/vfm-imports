@component('mail::message')
# Import Processing

Hello {{ $user->name }},

{{ $contacts_count }} contacts have finished importing for {{ $client_name }}! We will alert you when the import has finished.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
