@component('mail::message')
# Import Complete

Hello {{ $name }},

{{ $contacts_count }} contacts have finished importing for {{ $client_name }}! You may click the button below to view them.

@component('mail::button', ['url' => $url, 'color' => 'orange'])
View Contacts
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
