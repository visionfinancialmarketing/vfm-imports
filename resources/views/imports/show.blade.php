@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ $import->name }}
                    </div>

                    <div class="panel-body">
                        <p><strong>Path</strong>: {{ $import->path }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
