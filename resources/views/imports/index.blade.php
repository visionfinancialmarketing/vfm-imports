@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="far fa-cloud-upload"></i> Imports</div>

                    <div class="panel-body">
                        <ul class="">
                            @forelse ($imports as $import)
                                <li>
                                    {{ $import->name }}
                                </li>
                            @empty
                                <li><h1>There are no imports at this time.</h1></li>
                            @endforelse
                        </ul>

                        @if ( method_exists($imports, 'links') )
                            {{ $imports->links() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
