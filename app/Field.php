<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    public $table = "fields";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_notinuse_client_id', 'field', 'position'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];
}
