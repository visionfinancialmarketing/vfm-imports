<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'master', 'disabled', 'first_name', 'last_name', 'title', 'email', 'password', 'password_expiration', 'website', 'phone_number', 'fax_number', 'business_name', 'address', 'address_line_2', 'city', 'state', 'zip', 'timezone', 'emails_sent', 'automatic_logout', 'default_calendar_view', 'calendar_start_time', 'calendar_end_time', 'activity_default_time', 'headshot', 'signature', 'logo', 'category_colors', 'option_colors', 'email_signature', 'compliance', 'cart', 'preferences', 'appointment_number_max', 'deleted', 'renewal_day_of_month', 'deleted_date', 'billing_paid_account', 'billing_users_count', 'auto_renew_notification', 'billing_membership', 'billing_space_tier', 'billing_email_tier', 'billing_next_date', 'billing_next_date_previous', 'billing_last_date', 'billing_last_amount', 'billing_account_graced', 'last_failed_email_notice', 'last_failed_email_notice_flag', 'near_quota_email_notice', 'near_quota_email_notice_flag', 'unverified_email_notice', 'unverified_email_notice_flag', 'lock_count', 'lock_timestamp', 'remember_id', 'remember_hash', 'remember_key', 'remember_expires', 'reset_link', 'reset_expires', 'verified', 'verify_link', 'active', 'boldness'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];
}
