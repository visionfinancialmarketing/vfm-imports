<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomFieldContact extends Model
{
    public $table = "contacts_custom_fields";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contact_id', 'custom_field_id', 'value', 'timestamp'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];
}
