<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    public $table = "contacts_socials";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'contact_id', 'ssn', 'timestamp'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];
}
