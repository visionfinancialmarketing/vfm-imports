<?php

namespace App\Jobs;

use App\Tag;
use App\User;
use Exception;
use App\Import;
use App\Contact;
use Carbon\Carbon;
use App\TagContact;
use App\CustomField;
use App\FieldContact;
use App\RelationContact;
use App\CustomFieldContact;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use App\Notifications\ImportComplete;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportContacts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    public $user;
    public $client_id;
    public $created_by;
    public $csv;
    public $new_array;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, $client_id, $created_by, $csv, $new_array)
    {
        $this->user = $user;
        $this->client_id = $client_id;
        $this->created_by = $created_by;
        $this->csv = json_decode($csv, true);
        $this->new_array = json_decode($new_array, true);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $contacts_count = 0;
        foreach ($this->csv as $row) {
            // Set Contact Defaults...
            $classification = 'Client';
            $rating = 3;
            $status = '';
            $email_address = 0;
            $telephone_number = 0;
            $street_address = 0;
            $photo = '';
            $first_name = '';
            $middle_name = '';
            $last_name = '';
            $alternate_name = '';
            $maiden_name = '';
            $prefix = '';
            $suffix = '';
            $brief_description = '';
            $occupation_title = '';
            $occupation_company = '';
            $occupation_start = NULL;
            $occupation_end = NULL;
            $salary = '';
            $note = '';
            $birthdate = NULL;
            $birthplace = '';
            $date_of_death = NULL;
            $gender = '';
            $citizenship_country = '';
            $ethnicity = '';
            $marital_status = '';
            $anniversary_date = NULL;
            $retirement_status = '';
            $retirement_date = NULL;
            $tax_id_number = '';
            $pets = '';
            $dl_number = '';
            $dl_state = '';
            $dl_expiration_date = NULL;
            $source = '';
            $source_category = '';
            $source_notes = '';
            $client_timestamp = NULL;
            $record_timestamp = date('Y-m-d H:i:s', time());
            $office_primary_advisor = $this->client_id;
            $office_secondary_advisor = $this->created_by;
            $office_primary_csr = '';
            $office_secondary_csr = '';
            $office_location = '';
            $appointment_status = '';
            $appointment_number = 0;
            $hold_date = NULL;
            $current_plan = 0;
            $case_management = '';
            $personal_touch_workflow = '';
            $personal_touch_timestamp = NULL;
            $flagged = 0;
            $wrenched = 0;
            $num_children = 0;
            $finances = NULL;
            $homeownership = NULL;
            $fed_taxes = NULL;
            $state_taxes = NULL;
            $tax_year = NULL;
            $net_worth = '0.00';
            $last_access = NULL;
            $do_not_call = 0;
            $do_not_email = 0;
            $do_not_mail = 0;
            $ssn = '';
            $general_tags = [];
            $interest_tags = [];
            $phones = [];
            $emails = [];
            $address = [];
            $addresses = [];
            $this_address = 0;
            $address_count = 0;
            $custom_data = [];

            foreach ($this->new_array as $info) {
                if ($info["vfm_key"] === '' || $info["vfm_key"] === 'noooooo') continue;
                $row_value = $row[$info["key"]];

                if ($info["vfm_key"] === 'classification') {
                    if ($row_value !== '') {
                        $classification = $row_value;
                    }
                } else if ($info["vfm_key"] === 'rating') {
                    if ($row_value !== '') {
                        $rating = $row_value;
                    }
                } else if ($info["vfm_key"] === 'status') {
                    if ($row_value !== '') {
                        $status = $row_value;
                    }
                } else if ($info["vfm_key"] === 'first_name') {
                    if ($row_value !== '') {
                        $first_name = $row_value;
                    }
                } else if ($info["vfm_key"] === 'middle_name') {
                    if ($row_value !== '') {
                        $middle_name = $row_value;
                    }
                } else if ($info["vfm_key"] === 'last_name') {
                    if ($row_value !== '') {
                        $last_name = $row_value;
                    }
                } else if ($info["vfm_key"] === 'alternate_name') {
                    if ($row_value !== '') {
                        $alternate_name = $row_value;
                    }
                } else if ($info["vfm_key"] === 'maiden_name') {
                    if ($row_value !== '') {
                        $maiden_name = $row_value;
                    }
                } else if ($info["vfm_key"] === 'prefix') {
                    if ($row_value !== '') {
                        $prefix = $row_value;
                    }
                } else if ($info["vfm_key"] === 'suffix') {
                    if ($row_value !== '') {
                        $suffix = $row_value;
                    }
                } else if ($info["vfm_key"] === 'brief_description') {
                    if ($row_value !== '') {
                        $brief_description = $row_value;
                    }
                } else if ($info["vfm_key"] === 'occupation_title') {
                    if ($row_value !== '') {
                        $occupation_title = $row_value;
                    }
                } else if ($info["vfm_key"] === 'occupation_company') {
                    if ($row_value !== '') {
                        $occupation_company = $row_value;
                    }
                } else if ($info["vfm_key"] === 'occupation_start') {
                    if ($row_value !== '') {
                        $occupation_start = date('Y-m-d', strtotime($row_value));
                    }
                } else if ($info["vfm_key"] === 'occupation_end') {
                    if ($row_value !== '') {
                        $occupation_end = date('Y-m-d', strtotime($row_value));
                    }
                } else if ($info["vfm_key"] === 'salary') {
                    if ($row_value !== '') {
                        $salary = $row_value;
                    }
                } else if ($info["vfm_key"] === 'note') {
                    if ($row_value !== '') {
                        $note = $row_value;
                    }
                } else if ($info["vfm_key"] === 'birthdate') {
                    if ($row_value !== '') {
                        $birthdate = date('Y-m-d', strtotime($row_value));
                    }
                } else if ($info["vfm_key"] === 'birthplace') {
                    if ($row_value !== '') {
                        $birthplace = $row_value;
                    }
                } else if ($info["vfm_key"] === 'date_of_death') {
                    if ($row_value !== '') {
                        $date_of_death = date('Y-m-d', strtotime($row_value));
                    }
                } else if ($info["vfm_key"] === 'gender') {
                    if ($row_value !== '') {
                        $gender = $row_value;
                    }
                } else if ($info["vfm_key"] === 'citizenship_country') {
                    if ($row_value !== '') {
                        $citizenship_country = $row_value;
                    }
                } else if ($info["vfm_key"] === 'ethnicity') {
                    if ($row_value !== '') {
                        $ethnicity = $row_value;
                    }
                } else if ($info["vfm_key"] === 'marital_status') {
                    if ($row_value !== '') {
                        $marital_status = $row_value;
                    }
                } else if ($info["vfm_key"] === 'anniversary_date') {
                    if ($row_value !== '') {
                        $anniversary_date = date('Y-m-d', strtotime($row_value));
                    }
                } else if ($info["vfm_key"] === 'retirement_status') {
                    if ($row_value !== '') {
                        $retirement_status = $row_value;
                    }
                } else if ($info["vfm_key"] === 'retirement_date') {
                    if ($row_value !== '') {
                        $retirement_date = date('Y-m-d', strtotime($row_value));
                    }
                } else if ($info["vfm_key"] === 'tax_id_number') {
                    if ($row_value !== '') {
                        $tax_id_number = $row_value;
                    }
                } else if ($info["vfm_key"] === 'dl_number') {
                    if ($row_value !== '') {
                        $dl_number = $row_value;
                    }
                } else if ($info["vfm_key"] === 'dl_state') {
                    if ($row_value !== '') {
                        $dl_state = $row_value;
                    }
                } else if ($info["vfm_key"] === 'dl_expiration_date') {
                    if ($row_value !== '') {
                        $dl_expiration_date = date('Y-m-d', strtotime($row_value));
                    }
                } else if ($info["vfm_key"] === 'source') {
                    if ($row_value !== '') {
                        $source = $row_value;
                    }
                } else if ($info["vfm_key"] === 'client_timestamp') {
                    if ($row_value !== '') {
                        $client_timestamp = date('Y-m-d', strtotime($row_value));
                    }
                } else if ($info["vfm_key"] === 'record_timestamp') {
                    if ($row_value !== '') {
                        $record_timestamp = date('Y-m-d H:i:s', strtotime($row_value));
                    }
                } else if ($info["vfm_key"] === 'ssn') {
                    if ($row_value !== '') {
                        $social = str_replace('.', '', $row_value);
                        $social = str_replace('-', '', $social);
                        if ($social) {
                            $part1 = substr($social, 0, 3);
                            $part2 = substr($social, 3, 2);
                            $part3 = substr($social, 5);
                            $ssn = $part1."-".$part2."-".$part3;
                        }
                    }
                } else if ($info["vfm_key"] === 'general_tag') {
                    $general_tags = strpos($row_value, ',') !== false ? explode(', ', $row_value) : [];
                } else if ($info["vfm_key"] === 'interest_tag') {
                    $interest_tags = strpos($row_value, ',') !== false ? explode(', ', $row_value) : [];
                } else if ($info["vfm_key"] === 'phone') {
                    array_push($phones, ['value' => $row_value, 'type' => $info["type"], 'primary' => 0]);
                } else if ($info["vfm_key"] === 'email') {
                    array_push($emails, ['value' => $row_value, 'type' => $info["type"], 'primary' => 0]);
                } else if ($info["vfm_key"] === 'address' || $info["vfm_key"] === 'address2' || $info["vfm_key"] === 'city' || $info["vfm_key"] === 'state' || $info["vfm_key"] === 'zip') {
                    if ($info["vfm_key"] === 'address') $address_count++;
                    if (empty($address)) {
                        $this_address = $address_count;
                        $address[$info["vfm_key"]] = $row_value;
                        $address['type'] = $info["type"];
                        $address['count'] = $address_count;
                    } else if ($this_address == $address_count) {
                        $address[$info["vfm_key"]] = $row_value;
                    } else {
                        $this->insertAddress($address, $new_contact_id);
                        $address = [];
                        $this_address = $address_count;
                        $address[$info["vfm_key"]] = $row_value;
                        $address['type'] = $info["type"];
                        $address['count'] = $address_count;
                    }
                } else if ($info["vfm_key"] === 'custom') {
                    array_push($custom_data, ['name' => $info["key"], 'type' => $info["type"], 'value' => $row_value]);
                }
            }
            $contacts_count++;

            // Insert Contact...
            $new_contact_id = $this->insertContact($this->client_id, $this->created_by, $classification, $rating, $status, $email_address, $telephone_number, $street_address, $photo, $first_name, $middle_name, $last_name, $alternate_name, $maiden_name, $prefix, $suffix, $brief_description, $occupation_title, $occupation_company, $occupation_start, $occupation_end, $salary, $note, $birthdate, $birthplace, $date_of_death, $gender, $citizenship_country, $ethnicity, $marital_status, $anniversary_date, $retirement_status, $retirement_date, $tax_id_number, $pets, $dl_number, $dl_state, $dl_expiration_date, $source, $source_category, $source_notes, $client_timestamp, $record_timestamp, $office_primary_advisor, $office_secondary_advisor, $office_primary_csr, $office_secondary_csr, $office_location, $appointment_status, $appointment_number, $hold_date, $current_plan, $case_management, $personal_touch_workflow, $personal_touch_timestamp, $flagged, $wrenched, $num_children, $finances, $homeownership, $fed_taxes, $state_taxes, $tax_year, $net_worth, $last_access, $do_not_call, $do_not_email, $do_not_mail);

            // Insert Social Security Number
            if (!empty($ssn)) $this->insertSocial($ssn, $this->client_id, $new_contact_id);

            // Insert General Tags
            if (!empty($general_tags)) $this->insertTags($general_tags, 'general', $this->client_id, $new_contact_id);

            // Insert Interest Tags
            if (!empty($interest_tags)) $this->insertTags($interest_tags, 'interest', $this->client_id, $new_contact_id);

            // Insert Phones
            if (!empty($phones)) $this->insertFieldContacts($phones, 'telephone_number', $new_contact_id);

            // Insert Emails
            if (!empty($emails)) $this->insertFieldContacts($emails, 'email_address', $new_contact_id);

            // Insert Addresses
            if (!empty($address)) $this->insertAddress($address, $new_contact_id);

            // Insert Custom Data
            if (!empty($custom_data)) $this->insertCustomData($custom_data, $this->client_id, $new_contact_id);
        }

        // Link Relationships
        foreach ($this->csv as $row) {
            // dd($row);
            $first_name = '';
            $last_name = '';

            foreach ($this->new_array as $info) {
                if ($info["vfm_key"] === '' || $info["vfm_key"] === 'noooooo') continue;
                $row_value = $row[$info["key"]];
                // dd($row_value);

                if ($info["vfm_key"] === 'first_name') {
                    $first_name = $row_value;
                } else if ($info["vfm_key"] === 'last_name') {
                    $last_name = $row_value;
                }
            }

            $contact = Contact::where('first_name', $first_name)->where('last_name', $last_name)->first();
            if ($contact) {
                $custom_fields_for_contact = CustomField::select('id', 'name')->where('client_id', $contact->client_id)->get();
                if ($custom_fields_for_contact->isNotEmpty()) {
                    foreach ($custom_fields_for_contact as $custom_field) {
                        if (strpos(strtolower($custom_field), 'spouse') !== false) {
                            $check = CustomFieldContact::where('custom_field_id', $custom_field->id)->where('contact_id', $contact->id)->get();
                            if ($check->isNotEmpty()) {
                                $check = $check[0];
                                $spouse = DB::select("Select * FROM `vfm_contacts` WHERE concat(first_name, ' ', last_name) like :name", ['name' => $check['value']]);
                                if (count($spouse) > 0) {
                                    $spouse = $spouse[0];
                                    $this->linkSpouse($contact, $spouse);
                                }
                            }
                        } else continue;
                    }
                }
            }
        }

        $this->user->notify(new ImportComplete($this->user, $this->client_id, $contacts_count));
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        // event(new Flash('Oops! Something went wrong...', 'Import Failed', 'danger'));
    }

    /**
     * Insert Contact into Database.
     *
     * @param  \App\CRM  $crm
     * @return \Illuminate\Http\Response
     */
    private function insertContact($client_id, $created_by, $classification, $rating, $status, $email_address, $telephone_number, $street_address, $photo, $first_name, $middle_name, $last_name, $alternate_name, $maiden_name, $prefix, $suffix, $brief_description, $occupation_title, $occupation_company, $occupation_start, $occupation_end, $salary, $note, $birthdate, $birthplace, $date_of_death, $gender, $citizenship_country, $ethnicity, $marital_status, $anniversary_date, $retirement_status, $retirement_date, $tax_id_number, $pets, $dl_number, $dl_state, $dl_expiration_date, $source, $source_category, $source_notes, $client_timestamp, $record_timestamp, $office_primary_advisor, $office_secondary_advisor, $office_primary_csr, $office_secondary_csr, $office_location, $appointment_status, $appointment_number, $hold_date, $current_plan, $case_management, $personal_touch_workflow, $personal_touch_timestamp, $flagged, $wrenched, $num_children, $finances, $homeownership, $fed_taxes, $state_taxes, $tax_year, $net_worth, $last_access, $do_not_call, $do_not_email, $do_not_mail)
    {
        $new_contact = new Contact;
        $new_contact->client_id = $client_id;
        $new_contact->created_by = $created_by;
        $new_contact->classification = $classification;
        $new_contact->rating = $rating;
        $new_contact->status = $status;
        $new_contact->email_address = $email_address;
        $new_contact->telephone_number = $telephone_number;
        $new_contact->street_address = $street_address;
        $new_contact->photo = $photo;
        $new_contact->first_name = $first_name;
        $new_contact->middle_name = $middle_name;
        $new_contact->last_name = $last_name;
        $new_contact->alternate_name = $alternate_name;
        $new_contact->maiden_name = $maiden_name;
        $new_contact->prefix = $prefix;
        $new_contact->suffix = $suffix;
        $new_contact->brief_description = $brief_description;
        $new_contact->occupation_title = $occupation_title;
        $new_contact->occupation_company = $occupation_company;
        $new_contact->occupation_start = $occupation_start;
        $new_contact->occupation_end = $occupation_end;
        $new_contact->salary = $salary;
        $new_contact->note = $note;
        $new_contact->birthdate = $birthdate;
        $new_contact->birthplace = $birthplace;
        $new_contact->date_of_death = $date_of_death;
        $new_contact->gender = $gender;
        $new_contact->citizenship_country = $citizenship_country;
        $new_contact->ethnicity = $ethnicity;
        $new_contact->marital_status = $marital_status;
        $new_contact->anniversary_date = $anniversary_date;
        $new_contact->retirement_status = $retirement_status;
        $new_contact->retirement_date = $retirement_date;
        $new_contact->tax_id_number = $tax_id_number;
        $new_contact->pets = $pets;
        $new_contact->dl_number = $dl_number;
        $new_contact->dl_state = $dl_state;
        $new_contact->dl_expiration_date = $dl_expiration_date;
        $new_contact->source = $source;
        $new_contact->source_category = $source_category;
        $new_contact->source_notes = $source_notes;
        $new_contact->client_timestamp = $client_timestamp;
        $new_contact->record_timestamp = $record_timestamp;
        $new_contact->office_primary_advisor = $client_id;
        $new_contact->office_secondary_advisor = $created_by;
        $new_contact->office_primary_csr = $office_primary_csr;
        $new_contact->office_secondary_csr = $office_secondary_csr;
        $new_contact->office_location = $office_location;
        $new_contact->appointment_status = $appointment_status;
        $new_contact->appointment_number = $appointment_number;
        $new_contact->hold_date = $hold_date;
        $new_contact->current_plan = $current_plan;
        $new_contact->case_management = $case_management;
        $new_contact->personal_touch_workflow = $personal_touch_workflow;
        $new_contact->personal_touch_timestamp = $personal_touch_timestamp;
        $new_contact->flagged = $flagged;
        $new_contact->wrenched = $wrenched;
        $new_contact->num_children = $num_children;
        $new_contact->finances = $finances;
        $new_contact->homeownership = $homeownership;
        $new_contact->fed_taxes = $fed_taxes;
        $new_contact->state_taxes = $state_taxes;
        $new_contact->tax_year = $tax_year;
        $new_contact->net_worth = $net_worth;
        $new_contact->last_access = $last_access;
        $new_contact->do_not_call = $do_not_call;
        $new_contact->do_not_email = $do_not_email;
        $new_contact->do_not_mail = $do_not_mail;
        $new_contact->save();

        return $new_contact_id = $new_contact->id;
    }

    /**
     * Insert Social Security Number into Database.
     *
     * @param  \App\CRM  $crm
     * @return \Illuminate\Http\Response
     */
    private function insertSocial($ssn, $client_id, $contact_id) {
        DB::insert('insert into vfm_contacts_socials (client_id, contact_id, ssn, timestamp) values (:client_id, :contact_id, AES_ENCRYPT(:ssn, :key), UTC_TIMESTAMP)', ['client_id' => $client_id, 'contact_id' => $contact_id, 'ssn' => $ssn, 'key' => env('SOCIAL_KEY')]);
    }

    /**
     * Insert Tags into Database.
     *
     * @param  \App\CRM  $crm
     * @return \Illuminate\Http\Response
     */
    private function insertTags($tags, $tag_type, $client_id, $contact_id, $flagged = 0) {
        foreach ($tags as $tag) {
            $current_tag = Tag::where('tag', $tag)->first();
            if (empty($current_tag)) {
                $new_tag = new Tag;
                $new_tag->client_id = $client_id;
                $new_tag->tag_type = $tag_type;
                $new_tag->tag = $tag;
                $new_tag->flagged = $flagged;
                $new_tag->save();

                $new_tag_to_contact = new TagContact;
                $new_tag_to_contact->tag_id = $new_tag->id;
                $new_tag_to_contact->contact_id = $contact_id;
                $new_tag_to_contact->save();
            } else {
                $current_tag_to_contact = TagContact::where('tag_id', $current_tag->id)->where('contact_id', $contact_id)->first();
                if (empty($current_tag_to_contact)) {
                    $new_tag_to_contact = new TagContact;
                    $new_tag_to_contact->tag_id = $current_tag->id;
                    $new_tag_to_contact->contact_id = $contact_id;
                    $new_tag_to_contact->save();
                }
            }
        }
    }

    /**
     * Insert FieldContacts into Database.
     *
     * @param  \App\CRM  $crm
     * @return \Illuminate\Http\Response
     */
    private function insertFieldContacts($values, $dataType, $contact_id) {
        $values[0]["primary"] = 1;
        foreach ($values as $value) {
            if (!empty($value["value"])) {
                if ($dataType === 'telephone_number') {
                    $this_value = FieldContact::formatPhone($value["value"]);
                } else {
                    $this_value = $value["value"];
                }

                $new_field_contact = new FieldContact;
                $new_field_contact->field_id = $value["type"];
                $new_field_contact->contact_id = $contact_id;
                $new_field_contact->value = $this_value;
                $new_field_contact->save();

                if ($value["primary"] === 1) {
                    $contact = Contact::find($contact_id);
                    $contact[$dataType] = $new_field_contact->id;
                    $contact->save();
                }
            }
        }
    }

    /**
     * Insert Addresses into Database.
     *
     * @param  \App\CRM  $crm
     * @return \Illuminate\Http\Response
     */
    private function insertAddress($address, $contact_id) {
        if (isset($address['address']) && $address['address'] !== '') {
            if ($address['count'] === 1) $address["primary"] = 1;
            $address2 = isset($address['address2']) && $address['address2'] !== '' ? $address["address2"] : '';

            $new_address = new FieldContact;
            $new_address->field_id = $address["type"];
            $new_address->contact_id = $contact_id;
            $new_address->value = json_encode(["street" => $address["address"], "street2" => $address2, "city" => $address["city"], "state" => $address["state"], "zip" => $address["zip"]]);
            $new_address->street = $address["address"];
            $new_address->street2 = $address2;
            $new_address->city = $address["city"];
            $new_address->state = $address["state"];
            $new_address->zip = $address["zip"];
            $new_address->save();

            if ($address['primary'] === 1) {
                $contact = Contact::find($contact_id);
                $contact['street_address'] = $new_address->id;
                $contact->save();
            }
        }
    }

    /**
     * Insert Addresses into Database.
     *
     * @param  \App\CRM  $crm
     * @return \Illuminate\Http\Response
     */
    private function insertCustomData($custom_data, $client_id, $contact_id) {
        foreach ($custom_data as $data) {
            if ($data['value'] === '') continue;

            $current_custom_field = CustomField::where('client_id', $client_id)->where('name', $data['name'])->first();
            if (empty($current_custom_field)) {
                $new_custom_field = new CustomField;
                $new_custom_field->client_id = $client_id;
                $new_custom_field->name = $data['name'];
                $new_custom_field->type = $data['type'];
                $new_custom_field->save();

                $new_custom_field_to_contact = new CustomFieldContact;
                $new_custom_field_to_contact->contact_id = $contact_id;
                $new_custom_field_to_contact->custom_field_id = $new_custom_field->id;
                $new_custom_field_to_contact->value = $data['value'];
                $new_custom_field_to_contact->timestamp = Carbon::now();
                $new_custom_field_to_contact->save();
            } else {
                $current_custom_field_to_contact = CustomFieldContact::where('custom_field_id', $current_custom_field->id)->where('contact_id', $contact_id)->first();
                if (empty($current_custom_field_to_contact)) {
                    $new_custom_field_to_contact = new CustomFieldContact;
                    $new_custom_field_to_contact->contact_id = $contact_id;
                    $new_custom_field_to_contact->custom_field_id = $current_custom_field->id;
                    $new_custom_field_to_contact->value = $data['value'];
                    $new_custom_field_to_contact->timestamp = Carbon::now();
                    $new_custom_field_to_contact->save();
                }
            }
        }
    }

    /**
     * Link Relationships together.
     *
     * @param  \App\CRM  $crm
     * @return \Illuminate\Http\Response
     */
    private function linkSpouse($contact, $spouse) {
        $contact_exists = RelationContact::where('contact_id', $contact->id)->get();
        if ($contact_exists->isEmpty()) {
            $new_relation_contact = new RelationContact;
            $new_relation_contact->relation_id = 1;
            $new_relation_contact->contact_id = $contact->id;
            $new_relation_contact->relative_id = $spouse->id;
            $new_relation_contact->other_relation = '';
            $new_relation_contact->relative_name = $spouse->first_name.' '.$spouse->last_name;
            $new_relation_contact->relative_birthdate = $spouse->birthdate;
            $new_relation_contact->relative_description = $spouse->brief_description;
            $new_relation_contact->referral = 0;
            $new_relation_contact->save();
        }

        $spouse_exists = RelationContact::where('contact_id', $spouse->id)->get();
        if ($spouse_exists->isEmpty()) {
            $new_relation_spouse_contact = new RelationContact;
            $new_relation_spouse_contact->relation_id = 1;
            $new_relation_spouse_contact->contact_id = $spouse->id;
            $new_relation_spouse_contact->relative_id = $contact->id;
            $new_relation_spouse_contact->other_relation = '';
            $new_relation_spouse_contact->relative_name = $contact->first_name.' '.$contact->last_name;
            $new_relation_spouse_contact->relative_birthdate = $contact->birthdate;
            $new_relation_spouse_contact->relative_description = $contact->brief_description;
            $new_relation_spouse_contact->referral = 0;
            $new_relation_spouse_contact->save();
        }
    }
}
