<?php

namespace App\Http\Controllers;

use App\Contact;
use App\CustomField;
use App\FieldContact;
use App\RelationContact;
use App\CustomFieldContact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::paginate(20);
        // foreach ($contacts as $contact) {
        //     $email = FieldContact::select('value')->where('id', $contact->email_address)->where('contact_id', $contact->id)->first();
        //     $contact->primary_email = $email ? $email->value : '';

        //     $phone = FieldContact::select('value')->where('id', $contact->telephone_number)->where('contact_id', $contact->id)->first();
        //     $contact->primary_phone = $phone ? $phone->value : '';

        //     $address = FieldContact::select('value')->where('id', $contact->street_address)->where('contact_id', $contact->id)->first();
        //     $contact->primary_address = $address ? $address->value : '';
        // }

        return view('contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        $email = FieldContact::select('value')->where('id', $contact->email_address)->where('contact_id', $contact->id)->first();
        $contact->primary_email = $email ? $email->value : '';

        $phone = FieldContact::select('value')->where('id', $contact->telephone_number)->where('contact_id', $contact->id)->first();
        $contact->primary_phone = $phone ? $phone->value : '';

        $address = FieldContact::select('value')->where('id', $contact->street_address)->where('contact_id', $contact->id)->first();
        $contact->primary_address = $address ? $address->value : '';

        $stmt = DB::select('SELECT AES_DECRYPT(ssn, :key) as `ssn` FROM `vfm_contacts_socials` WHERE `client_id` = :client_id AND `contact_id` = :contact_id LIMIT 1', ['key' => env('SOCIAL_KEY'), 'client_id' => $contact->client_id, 'contact_id' => $contact->id]);
        if (count($stmt) > 0) {
            $contact->ssn_full = $stmt[0]->ssn;
            $ssn_array = explode('-', $stmt[0]->ssn);
            $contact->last4 = !empty($ssn_array) ? end($ssn_array) : '';
        }

        $spouse_id = RelationContact::select('relative_id')->where('contact_id', $contact->id)->first();
        if (count($spouse_id) > 0) {
            $contact->spouse = Contact::find($spouse_id->relative_id);
        }

        $custom_data = CustomFieldContact::where('contact_id', $contact->id)->get();
        if ($custom_data->isNotEmpty()) {
            foreach ($custom_data as $data) {
                $data->label = CustomField::select('name')->where('id', $data->custom_field_id)->first()->name;
            }
            $contact->custom_data = $custom_data;
        }

        return view('contacts.show', compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        //
    }
}
