<?php

namespace App\Http\Controllers;

use App\CRM;
use App\Tag;
use App\Field;
use App\Client;
use App\Import;
use App\Social;
use App\Contact;
use Carbon\Carbon;
use App\TagContact;
use App\CustomField;
use App\FieldContact;
use App\RelationContact;
use App\CustomFieldContact;
use App\Jobs\ImportContacts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Notifications\ImportProcessing;
use Illuminate\Support\Facades\Storage;

class ImportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeImports(Request $request)
    {
        $crm = CRM::find($request->crm)->name;
        $main_directory = 'public/CRMs';
        $file_directory = $main_directory.'/'.$crm;
        $main_directory_exists = Storage::directories($main_directory);
        if (!$main_directory_exists) {
            Storage::makeDirectory($main_directory);
            Storage::makeDirectory($file_directory);
        } else {
            $file_directory_exists = Storage::directories($file_directory);
            if (!$file_directory_exists) {
                Storage::makeDirectory($file_directory);
            }
        }

        $original_name = $request->file->getClientOriginalName();
        $path = Storage::putFileAs($file_directory, $request->file('file'), $original_name);
        $path = str_replace('public', 'storage', $path);

        $import = new Import;
        $import->crm_id = (int)$request->crm;
        $import->name = $original_name;
        $import->path = $path;
        $import->size = $request->file->getClientSize();
        $import->type = $request->file->getClientMimeType();
        $import->ext = $request->file->getClientOriginalExtension();
        $import->save();

        $files = Import::where('crm_id', (int)$request->crm)->get();
        foreach ($files as $file) {
            $csv = array_map('str_getcsv', file($file->path));
            $column_headers = $csv[0];
            $file->headers = $column_headers;
        }

        return response()->json(['success' => true, 'files' => $files]);
    }

    /**
     * import a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function importContacts(Request $request)
    {
        $user = Auth::user();
        $client_id = $request->client_id;
        $client_name = Client::find($client_id)->name;
        $created_by = $request->created_by;

        $current_file = Import::find($request->file_id);
        $csv = array_map('str_getcsv', file($current_file->path));
        $column_headers = $csv[0];
        $column_headers_request = str_replace(' ', '_', $csv[0]);
        array_walk($csv, function(&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });
        array_shift($csv);

        $new_array = [];
        for ($i = 0; $i < count($column_headers_request); $i++) {
            $new_array[] = ['key' => $column_headers[$i], 'vfm_key' => $request[$column_headers_request[$i]], 'type' => $request[$column_headers_request[$i].'-type']];
        }

        ImportContacts::dispatch($user, $client_id, $created_by, json_encode($csv), json_encode($new_array));
        $user->notify(new ImportProcessing($user, $client_id, count($csv)));

        return response()->json(['success' => true, 'name' => $user->name, 'contacts_count' => count($csv), 'client_name' => $client_name]);
    }

    /**
     * Display the specified resource.
     *
     * @param  $crm_id
     * @return \Illuminate\Http\Response
     */
    public function showImports($crm_id)
    {
        $files = Import::where('crm_id', (int)$crm_id)->get();
        foreach ($files as $file) {
            $csv = array_map('str_getcsv', file($file->path));
            $column_headers = $csv[0];
            $file->headers = $column_headers;
        }

        return response()->json(['success' => true, 'files' => $files]);
    }

    /**
     * Reset the Database.
     *
     * @return \Illuminate\Http\Response
     */
    public function resetLocalImports()
    {
        DB::table('contacts')->truncate();
        DB::table('contacts_custom_fields')->truncate();
        DB::table('contacts_socials')->truncate();
        DB::table('fields_to_contacts')->truncate();
        DB::table('relations_to_contacts')->truncate();
        DB::table('tags_to_contacts')->truncate();

        DB::table('jobs')->truncate();
        DB::table('failed_jobs')->truncate();

        return redirect('contacts');
    }

    /**
     * Get a list of Field Types.
     *
     * @param  $crm_id
     * @return \Illuminate\Http\Response
     */
    public function getFieldTypes($field_type)
    {
        $fields = Field::select('field as text', 'id as value')->where('field_type', $field_type)->orderBy('position', 'asc')->get();
        foreach ($fields as $field) {
            $field->text = ucwords(str_replace('_', ' ', $field->text));
        }
        return response()->json(['success' => true, 'fields' => $fields]);
    }

    /**
     * Remove the specified resource from storage and database.
     *
     * @param  \App\CRM  $crm
     * @return \Illuminate\Http\Response
     */
    public function destroyImport(Request $request)
    {
        $current_file = Import::find($request->file_id);
        $crm_id = $current_file->crm_id;

        Storage::delete(str_replace('storage', 'public', $current_file->path));
        $current_file->delete();

        $files = Import::where('crm_id', $crm_id)->get();
        foreach ($files as $file) {
            $csv = array_map('str_getcsv', file($file->path));
            $column_headers = $csv[0];
            $file->headers = $column_headers;
        }

        return response()->json(['success' => true, 'files' => $files]);
    }
}
