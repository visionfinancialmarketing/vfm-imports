<?php

namespace App\Http\Controllers;

use App\CRM;
use Illuminate\Http\Request;

class CRMController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $crms = CRM::orderBy('name')->paginate(10);
        return view('crms.index', compact('crms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        CRM::create($request->all());
        return redirect()->route('crms.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CRM  $crm
     * @return \Illuminate\Http\Response
     */
    public function show(CRM $crm)
    {
        return view('crms.show', compact('crm'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CRM  $crm
     * @return \Illuminate\Http\Response
     */
    public function edit(CRM $crm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CRM  $crm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CRM $crm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CRM  $crm
     * @return \Illuminate\Http\Response
     */
    public function destroy(CRM $crm)
    {
        //
    }
}
