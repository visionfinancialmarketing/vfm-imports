<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'classification', 'rating', 'status', 'email_address', 'telephone_number', 'street_address', 'photo', 'first_name', 'middle_name', 'last_name', 'alternate_name', 'maiden_name', 'prefix', 'suffix', 'brief_description', 'occupation_title', 'occupation_company', 'occupation_start', 'occupation_end', 'salary', 'note', 'birthdate', 'birthplace', 'date_of_death', 'gender', 'citizenship_country', 'ethnicity', 'marital_status', 'anniversary_date', 'retirement_status', 'retirement_date', 'tax_id_number', 'pets', 'dl_number', 'dl_state', 'dl_expiration_date', 'source', 'source_category', 'source_notes', 'client_timestamp', 'record_timestamp', 'office_primary_advisor', 'office_secondary_advisor', 'office_primary_csr', 'office_secondary_csr', 'office_location', 'appointment_status', 'appointment_number', 'hold_date', 'current_plan', 'case_management', 'personal_touch_workflow', 'personal_touch_timestamp', 'flagged', 'wrenched', 'num_children', 'finances', 'homeownership', 'fed_taxes', 'state_taxes', 'tax_year', 'net_worth', 'last_access', 'do_not_call', 'do_not_email', 'do_not_mail'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];
}
