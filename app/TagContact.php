<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagContact extends Model
{
    public $table = "tags_to_contacts";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tag_id', 'contact_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];
}
