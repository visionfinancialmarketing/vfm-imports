<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomField extends Model
{
    public $table = "custom_fields";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'name', 'type', 'values'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];
}
