<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelationContact extends Model
{
    public $table = "relations_to_contacts";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'relation_id', 'contact_id', 'relative_id', 'other_relation', 'relative_name', 'relative_birthdate', 'relative_description', 'referral', 'referral_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];
}
