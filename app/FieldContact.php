<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FieldContact extends Model
{
    public $table = "fields_to_contacts";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'field_id', 'contact_id', 'value', 'street', 'street2', 'city', 'state', 'zip'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * Insert FieldContacts into Database.
     *
     * @param  \App\CRM  $crm
     * @return \Illuminate\Http\Response
     */
    public static function formatPhone($phone) {
        $new_phone = '';
        $phone = str_replace('(', '', $phone);
        $phone = str_replace(')', '', $phone);
        $phone = str_replace('-', '', $phone);
        $phone = str_replace(' ', '', $phone);
        if (strlen($phone) > 10) $new_phone = substr(strtolower($phone), 10, 3) === "ext" ? explode('ext', strtolower($phone)) : substr(strtolower($phone), 11, 3) === "ext" ? explode('ext', strtolower($phone)) : explode('x', strtolower($phone));
        if (!empty($new_phone)) {
            if (substr(strtolower($new_phone[0]), 10, 2) === "or") {
                $new_new_phone = explode('or', strtolower($new_phone[0]));
                if (strlen($new_new_phone[0]) === 11 && strlen($new_new_phone[1]) === 11) {
                    $part1 = substr($new_new_phone[0], 0, 1);
                    $part2 = substr($new_new_phone[0], 1, 3);
                    $part3 = substr($new_new_phone[0], 4, 3);
                    $part4 = substr($new_new_phone[0], 7);
                    $part5 = substr($new_new_phone[1], 0, 1);
                    $part6 = substr($new_new_phone[1], 1, 3);
                    $part7 = substr($new_new_phone[1], 4, 3);
                    $part8 = substr($new_new_phone[1], 7);
                    $phone = $part1." (".$part2.") ".$part3."-".$part4."|".$part5." (".$part6.") ".$part7."-".$part8;
                } else if (strlen($new_new_phone[0]) === 11 && strlen($new_new_phone[1]) === 10) {
                    $part1 = substr($new_new_phone[0], 0, 1);
                    $part2 = substr($new_new_phone[0], 1, 3);
                    $part3 = substr($new_new_phone[0], 4, 3);
                    $part4 = substr($new_new_phone[0], 7);
                    $part5 = substr($new_new_phone[1], 0, 3);
                    $part6 = substr($new_new_phone[1], 3, 3);
                    $part7 = substr($new_new_phone[1], 6);
                    $phone = $part1." (".$part2.") ".$part3."-".$part4."|(".$part5.") ".$part6."-".$part7;
                } else if (strlen($new_new_phone[0]) === 10 && strlen($new_new_phone[1]) === 11) {
                    $part1 = substr($new_new_phone[0], 0, 3);
                    $part2 = substr($new_new_phone[0], 3, 3);
                    $part3 = substr($new_new_phone[0], 6);
                    $part4 = substr($new_new_phone[1], 0, 1);
                    $part5 = substr($new_new_phone[1], 1, 3);
                    $part6 = substr($new_new_phone[1], 4, 3);
                    $part7 = substr($new_new_phone[1], 7);
                    $phone = "(".$part1.") ".$part2."-".$part3."|".$part4." (".$part5.") ".$part6."-".$part7;
                } else {
                    $part1 = substr($new_new_phone[0], 0, 3);
                    $part2 = substr($new_new_phone[0], 3, 3);
                    $part3 = substr($new_new_phone[0], 6);
                    $part4 = substr($new_new_phone[1], 0, 3);
                    $part5 = substr($new_new_phone[1], 3, 3);
                    $part6 = substr($new_new_phone[1], 6);
                    $phone = "(".$part1.") ".$part2."-".$part3."|(".$part4.") ".$part5."-".$part6;
                }
            } else {
                if (strlen($new_phone[0]) === 11) {
                    $part1 = substr($new_phone[0], 0, 1);
                    $part2 = substr($new_phone[0], 1, 3);
                    $part3 = substr($new_phone[0], 4, 3);
                    $part4 = substr($new_phone[0], 7);
                    $phone = $part1." (".$part2.") ".$part3."-".$part4." Ext. ".$new_phone[1];
                } else {
                    $part1 = substr($new_phone[0], 0, 3);
                    $part2 = substr($new_phone[0], 3, 3);
                    $part3 = substr($new_phone[0], 6);
                    $phone = "(".$part1.") ".$part2."-".$part3." Ext. ".$new_phone[1];
                }
            }

            return $phone;
        } else {
            if (!empty($phone)) {
                $new_phone = $phone;
                $part1 = substr($new_phone, 0, 3);
                $part2 = substr($new_phone, 3, 3);
                $part3 = substr($new_phone, 6);
                $phone = "(".$part1.") ".$part2."-".$part3;
            }

            return $phone;
        }

        return '';
    }
}
