<?php

namespace App\Listeners;

use App\Events\Flash;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Session;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FlashListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Flash  $event
     * @return void
     */
    public function handle(Flash $event)
    {

        // $this->session->flash('flash', [ $event->message, $event->level ]);
    }
}
