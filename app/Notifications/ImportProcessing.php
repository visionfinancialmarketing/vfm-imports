<?php

namespace App\Notifications;

use App\User;
use App\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class ImportProcessing extends Notification implements ShouldQueue
{
    use Queueable;

    public $user;
    public $client_id;
    public $count;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, $client_id, $count)
    {
        $this->user = $user;
        $this->client_id = $client_id;
        $this->count = $count;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        $contacts_count = $this->count;
        $client = Client::find($this->client_id);
        $client_name = $client->business_name ? $client->business_name : $client->first_name+' '+$client->last_name;

        return (new SlackMessage)
                    ->success()
                    ->from('VFM Imports')
                    ->image('https://www.visionfinancialmarketing.com/dashboard/images/Orange-Target.png')
                    ->content($contacts_count.' contacts have started importing for '.$client_name.'! We will alert you when the import has finished.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
