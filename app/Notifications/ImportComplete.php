<?php

namespace App\Notifications;

use App\User;
use App\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class ImportComplete extends Notification implements ShouldQueue
{
    use Queueable;

    public $user;
    public $client_id;
    public $count;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, $client_id, $count)
    {
        $this->user = $user;
        $this->client_id = $client_id;
        $this->count = $count;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'slack', 'mail'];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        $name = $this->user->name;
        $url = url('/contacts');
        $contacts_count = $this->count;
        $client = Client::find($this->client_id);
        $client_name = $client->business_name ? $client->business_name : $client->first_name+' '+$client->last_name;

        return new BroadcastMessage([
            'message' => '<div>Hello '.$name.',</div><div>'.$contacts_count.' contacts have finished importing for '.$client_name.'!</div><div>You may <a href="'.$url.'">click here</a> to view your Contacts.</div>',
            'level' => 'success',
            'hide' => false
        ]);
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        $url = url('/contacts');
        $contacts_count = $this->count;
        $client = Client::find($this->client_id);
        $client_name = $client->business_name ? $client->business_name : $client->first_name+' '+$client->last_name;

        return (new SlackMessage)
                    ->success()
                    ->from('VFM Imports')
                    ->image('https://www.visionfinancialmarketing.com/dashboard/images/Orange-Target.png')
                    ->content("Import Complete!")
                    ->attachment(function ($attachment) use ($url, $contacts_count, $client_name) {
                        $attachment->title('View Contacts', $url)
                                   ->fields([
                                        'Count' => $contacts_count,
                                        'Client' => $client_name,
                                    ]);
                    });
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('/contacts');
        $contacts_count = $this->count;
        $client = Client::find($this->client_id);
        $client_name = $client->business_name ? $client->business_name : $client->first_name+' '+$client->last_name;

        return (new MailMessage)
                    ->subject('Import Completed')
                    ->markdown('mail.import.complete', ['url' => $url, 'name' => $this->user->name, 'contacts_count' => $contacts_count, 'client_name' => $client_name]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
